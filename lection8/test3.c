
/*
 * Copyright (c) 2017, GlobalLogic Ukraine LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the GlobalLogic.
 * 4. Neither the name of the GlobalLogic nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY GLOBALLOGIC UKRAINE LLC ``AS IS`` AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GLOBALLOGIC UKRAINE LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Define prior to inclusion of any Linux Kernel header file.
 * This specifies prefix used by pr_*() family functions.
 */
#define pr_fmt(fmt)	KBUILD_MODNAME ": " fmt

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/bug.h>
#include <linux/module.h>
#include <linux/export.h>
#include <linux/string.h>
#include <linux/cpu.h>
#include <linux/atomic.h>
#include <linux/bitops.h>
#include <linux/workqueue.h>
#include <linux/slab.h>

/* Needed for msleep() in Unit-Test */
#include <linux/delay.h>

#include <test3.h>

#define VERSION "1.0"

MODULE_AUTHOR("Serhii Popovych <serhii.popovych@globallogic.com>");
MODULE_DESCRIPTION("Test (3) (FSM using atomic bitops) in Linux Kernel Training");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_VERSION(VERSION);

/* All following stuff needed only to schedule tasklets
 * on dedicated CPUs since they schedule only on CPU
 * where tasklet_schedule() is called, but kept as generic
 * as possible for use in other scenarious.
 *
 * In real life tasklets might be scheduled on same CPU
 * as Interrupt Service Routine (ISR) is running.
 *
 * For demonstration purposes only we implement serialized
 * access to schedule/finish routines using atomic bit
 * operations on @state bits in struct job_fsm_data.
 */

/* FSM states */
enum {
#define JFD_STATE_INIT		JFD_STATE_INIT
	JFD_STATE_INIT		= (1 << 0),
#define JFD_STATE_SCHEDULE	JFD_STATE_SCHEDULE
	JFD_STATE_SCHEDULE	= (1 << 1),
#define JFD_STATE_RUNNING	JFD_STATE_RUNNING
	JFD_STATE_RUNNING	= (1 << 2),
#define JFD_STATE_FINISH	JFD_STATE_FINISH
	JFD_STATE_FINISH	= (1 << 3),
};

struct job_fsm_data {
	unsigned long		state;		/* bitfield of JFD_STATE_* */
	job_on_func_t		start_job_on;	/* run on job start */
	job_on_func_t		stop_job_on;	/* run on job stop */
	struct work_struct	work;		/* work to start on given CPU */
};

static void start_job_on_cpu(struct work_struct *work)
{
	struct job_fsm_data *jfd =
		container_of(work, struct job_fsm_data, work);
	unsigned int cpu;

	/* Scheduled via workqueue: thus can be preempted
	 * at any time. Thus it is racy to do various
	 * checks and scheduling multiple jobs in ->start_job_on()
	 * because one job might start on one CPU and another
	 * one on different CPU from the previous.
	 */
	preempt_disable();

	/* It contains debugging code to catch usage in
	 * preemptible context where CPU could change.
	 * It is safe here because preemption is disabled.
	 */
	cpu = smp_processor_id();

	pr_debug("start job cpu: %d\n", cpu);

	/* Support code reentrancy. In case of we called
	 * on live instance directly (i.e. without
	 * schedule/finish FSM) do not reschedule tasklet
	 * and delayed work.
	 *
	 * When called via FSM this should never happen and
	 * therefore this check is mostly for demonstration
	 * purposes too.
	 */
	if (test_and_set_bit(JFD_STATE_RUNNING, &jfd->state))
		goto out;

	/* Now perform caller specific job startup. */
	jfd->start_job_on(cpu);
out:
	preempt_enable();
}

/* This routine is reentrant but not thread-safe! It can be entered
 * from multiple execution flows with the same @cpu and jobs[].
 * However multiple threads can't use it because of shared jobs[].
 * Atomic bit operations guard from rescheduling via sched_work_on()
 * on live data thus making code thread-safe via serialization.
 *
 * Caller should use finish_job_on_cpu() to make this function
 * schedule job again.
 *
 * After initialization via init_jobs() this is only routine that
 * can be called thus implementing job scheduling machine finite
 * states.
 */
void schedule_job_on_cpu(unsigned int cpu, jfd_handle_t jobs)
{
	struct job_fsm_data *jfd = &((struct job_fsm_data *)jobs)[cpu];

	BUG_ON(!test_bit(JFD_STATE_INIT, &jfd->state));

	if (!test_and_set_bit(JFD_STATE_SCHEDULE, &jfd->state)) {
		/* Note that workqueue always contain only single
		 * job instance. Thus there is no need to guard this
		 * using atomic bitops in reality.
		 *
		 * However we implementing FSM for demonstrational
		 * purposes and thus we do checks using atomic bitops.
		 *
		 * Note that while &jfd->work is queued for immediate
		 * execution it does not mean it will start running
		 * immediately after schedule_work_on() returns.
		 *
		 * It might happen that before work dequeued for
		 * run an hot-remove event occurs that moves work
		 * to queue on different CPU. Thus corresponding
		 * checks is assed to work to avoid scheduling
		 * jobs on CPU other than we schedule initially.
		 *
		 * Thus caller of schedule_job_on_cpu() must ensure
		 * that jobs really started before continue processing.
		 */
		schedule_work_on(cpu, &jfd->work);
		clear_bit(JFD_STATE_FINISH, &jfd->state);
	}
}
EXPORT_SYMBOL(schedule_job_on_cpu);

/* This routine is reentrant but not thread-safe! It can be entered
 * from multiple execution flows with the same @cpu and jobs[].
 * However multiple threads can't use it because of shared jobs[].
 *
 * Caller might schedule_job_on_cpu() again after return from
 * this function.
 *
 * After initialization via init_jobs() it is not possible to
 * run any actions except BUG_ON() test that checks for initialized
 * data and test_and_set_bit() which returns 1 until job is
 * scheduled via schedule_job_on_cpu().
 */
void finish_job_on_cpu(unsigned int cpu, jfd_handle_t jobs)
{
	struct job_fsm_data *jfd = &((struct job_fsm_data *)jobs)[cpu];

	BUG_ON(!test_bit(JFD_STATE_INIT, &jfd->state));

	if (test_and_set_bit(JFD_STATE_FINISH, &jfd->state))
		return;

	/* Just in case populate work scheduled, but
	 * not started and we decided to stop everything.
	 *
	 * Note that "for" is used instead of "on" in
	 * debugging messages since we might run preemptible
	 * and CPU running this might differ from one
	 * we finish operation for.
	 */
	pr_debug("canceling job for cpu%u\n", cpu);
	cancel_work_sync(&jfd->work);

	if (test_and_clear_bit(JFD_STATE_RUNNING, &jfd->state)) {
		pr_debug("stopping job for cpu%u\n", cpu);
		jfd->stop_job_on(cpu);
	}

	clear_bit(JFD_STATE_SCHEDULE, &jfd->state);
}
EXPORT_SYMBOL(finish_job_on_cpu);

bool is_job_running(unsigned int cpu, jfd_handle_t jobs)
{
	struct job_fsm_data *jfd = &((struct job_fsm_data *)jobs)[cpu];

	return test_bit(JFD_STATE_RUNNING, &jfd->state);
}
EXPORT_SYMBOL(is_job_running);

jfd_handle_t init_jobs(job_on_func_t start, job_on_func_t stop, gfp_t gfp_mask)
{
	unsigned int cpu;
	struct job_fsm_data *jobs;

	if (!start || !stop)
		return JFD_HANDLE_INVALID;

	jobs = kmalloc_array(num_online_cpus(), sizeof(*jobs), gfp_mask);
	if (!jobs)
		return JFD_HANDLE_INVALID;

	for_each_online_cpu(cpu) {
		struct job_fsm_data *jfd = &jobs[cpu];

		INIT_WORK(&jfd->work, start_job_on_cpu);

		jfd->state = 0;
		jfd->start_job_on = start;
		jfd->stop_job_on  = stop;

		/* Allow reentrancy, but prohibit reinitialization.
		 * If JFD_STATE_INIT is set it should never be cleared.
		 * Thus if multiple execution flows enter same place
		 * test_and_set_bit() will always return 1 after first
		 * initialization (when JFD_STATE_INIT is zero).
		 */
		__set_bit(JFD_STATE_INIT, &jfd->state);
	}

	/*
	 * if everything is initialised correctly, try to get module.
	 */
	if (!try_module_get(THIS_MODULE)) {
		kfree(jobs);
		return JFD_HANDLE_INVALID;
	}

	return jobs;
}
EXPORT_SYMBOL(init_jobs);

void fini_jobs(jfd_handle_t jobs)
{
	unsigned int cpu;
	unsigned int finished = 0;

	if (!jobs)
		return;

	/* Assume everything that use jobs[] since init_jobs()
	 * is stopped using it. If not - bug.
	 */
	for_each_online_cpu(cpu) {
		struct job_fsm_data *jfd = &((struct job_fsm_data *)jobs)[cpu];
		unsigned long *state = &jfd->state;

		finished += test_bit(JFD_STATE_SCHEDULE, state);
		finished += test_bit(JFD_STATE_RUNNING, state);
		finished += !test_bit(JFD_STATE_FINISH, state);
	}

	/* Jobs are still in use */
	if (WARN_ON(finished))
		return;

	kfree(jobs);

	module_put(THIS_MODULE);
}
EXPORT_SYMBOL(fini_jobs);

/* Unit-Test */

static unsigned long ut_func_nr_calls __initdata;

static jfd_handle_t ut_jobs __initdata = JFD_HANDLE_INVALID;

static void __init ut_func(struct work_struct *work)
{
	unsigned int cpu;

	if (ut_jobs == JFD_HANDLE_INVALID)
		return;

	cpu = smp_processor_id();

	/* It contains debugging code to catch usage in
	 * preemptible context where CPU could change.
	 * It is safe here because preemption is disabled.
	 */

	ut_func_nr_calls++;
	if (likely(is_job_running(cpu, ut_jobs)))
		schedule_work_on(cpu, work);
}

static __initdata DECLARE_WORK(ut_work, ut_func);

static void __init start_jobs_on_cpu(unsigned int cpu)
{
	pr_debug("UT: scheduling job ->start_job_on() is called!cpu:%u\n", cpu);
	schedule_work(&ut_work);
}

static void __init stop_jobs_on_cpu(unsigned int cpu)
{
	pr_debug("UT: finishing job ->stop_job_on() is called!\n");
	flush_work(&ut_work);
	pr_debug("UT: ut_func_nr_calls == %lu\n", ut_func_nr_calls);
	BUG_ON(!ut_func_nr_calls);
}

static int __init test3_init(void)
{
	unsigned int cpu;

	/* A note about C strings used in functions marked with
	 * __init: they placed in .rodata section instead of __initdata
	 * therefore they not removed after load finishes and
	 * init sections removed. Thus they would consume additional
	 * space in .rodata.
	 */
	pr_debug("%s Unit-Test (UT)\n", "Starting");

	ut_jobs = init_jobs(start_jobs_on_cpu, stop_jobs_on_cpu, GFP_KERNEL);
	if (ut_jobs == JFD_HANDLE_INVALID) {
		pr_debug("UT: failed to init_jobs()");
		return -EINVAL;
	}

	for_each_online_cpu(cpu)
		schedule_job_on_cpu(cpu, ut_jobs);

	msleep(100);

	for_each_online_cpu(cpu)
		finish_job_on_cpu(cpu, ut_jobs);

	fini_jobs(ut_jobs);

	pr_debug("%s Unit-Test (UT)\n", "Finished");

	return 0;
}

static void __exit test3_exit(void)
{
	/* This is to support module unloading. */
}

module_init(test3_init);
module_exit(test3_exit);
