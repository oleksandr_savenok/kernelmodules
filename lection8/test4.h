
/*
 * Copyright (c) 2017, GlobalLogic Ukraine LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the GlobalLogic.
 * 4. Neither the name of the GlobalLogic nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY GLOBALLOGIC UKRAINE LLC ``AS IS`` AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GLOBALLOGIC UKRAINE LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _TEST4_H
#define _TEST4_H

#include <linux/types.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#ifdef USE_SPINLOCK
#include <linux/spinlock.h>
#endif

/* Items processed by producer/consumer */
struct my_data_list {
	struct list_head	list;
	u32			val;
};

/* per-CPU data head */
struct my_data_head {
	struct tasklet_struct	tasklet;	/* tasklet data struct */
	struct delayed_work	dwork;		/* delayed work data struct */
	struct list_head	head;		/* head of the my_data_list */
	unsigned long		nr_items;	/* number of items in @head */
#ifdef USE_SPINLOCK
	spinlock_t		lock;		/* serialize access to items */
#endif
};

/* How do we protect shared data between tasklet and workqueue?
 *
 * We share @head and @nr_items in struct my_data_head between
 * producing tasklet and consuming delayed work.
 */

#if   defined(USE_SPINLOCK)
/* This variant is only for demonstrational purposes on how spinlocks
 * can be used across multiple kernel contexts.
 *
 * More optimal implementation using tasklet enable/disable is default
 * when no other implementation specified.
 */

static inline void exclusive_lock_in_tasklet(struct my_data_head *mdh)
{
	spin_lock(&mdh->lock);
}

static inline void exclusive_unlock_in_tasklet(struct my_data_head *mdh)
{
	spin_unlock(&mdh->lock);
}

static inline void exclusive_lock_in_dwork(struct my_data_head *mdh)
{
	/* spin_lock_bh() is functionally equivalent to
	 *  local_bh_disable();
	 *  spin_lock();
	 *
	 * Why in tasklet we can use plain spin_lock() while
	 * in work we need to disable softirqs?
	 */
	spin_lock_bh(&mdh->lock);
}

static inline void exclusive_unlock_in_dwork(struct my_data_head *mdh)
{
	spin_unlock_bh(&mdh->lock);
}
#elif defined(USE_DISABLE_BH)
static inline void exclusive_lock_in_tasklet(struct my_data_head *mdh)
{
	/* no-op is here: answer why is the same as for
	 * exclusive_lock_in_dwork() for USE_SPINLOCK define.
	 */
}

static inline void exclusive_unlock_in_tasklet(struct my_data_head *mdh)
{
}

static inline void exclusive_lock_in_dwork(struct my_data_head *mdh)
{
	local_bh_disable();

	/* It seems tasklet_unlock_wait() isn't preempt safe. See default
	 * implementation using tasklet_disable()/tasklet_enable() below.
	 */
	preempt_disable();

	/* Q: why just disabling softirq on local CPU in work isn't enough? */
	tasklet_unlock_wait(&mdh->tasklet);

	preempt_enable();
}

static inline void exclusive_unlock_in_dwork(struct my_data_head *mdh)
{
	local_bh_enable();
}
#else
/* This is optimal for speed and simplicity approach when synchronizing
 * tasklet with works. Note that this is only possible because my_data_head
 * is per-CPU and actually synchronization between tasklet and work on same
 * CPU is needed.
 */
static inline void exclusive_lock_in_tasklet(struct my_data_head *mdh)
{
	/* Disable/Enable tasklet from itself useless because tasklets
	 * are not reentrant and never run simultaneously on multiple
	 * CPUs (i.e. when tasklet is running on one CPU it may be scheduled
	 * on another one but will not start running until currently
	 * running instance finishes).
	 *
	 * tasklet_disable() must follow exact number of tasklet_enable()
	 * to work correctly.
	 */
}

static inline void exclusive_unlock_in_tasklet(struct my_data_head *mdh)
{
}

static inline void exclusive_lock_in_dwork(struct my_data_head *mdh)
{
	/* Why exactly we need preemption here isn't known to me for now.
	 * While looking at tasklet_unlock_wait() it does atomic test_bit()
	 * and iterates while it is set. barrier() is used to instruct
	 * compiler to avoid reordering/optimization of the loop.
	 */
	preempt_disable();

	/* This will wait for running tasklet to finish via
	 * tasklet_unlock_wait() before return.
	 */
	tasklet_disable(&mdh->tasklet);

	preempt_enable();
}

static inline void exclusive_unlock_in_dwork(struct my_data_head *mdh)
{
	tasklet_enable(&mdh->tasklet);
}
#endif

#endif /* _TEST4_H */
