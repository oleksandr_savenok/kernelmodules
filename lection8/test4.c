
/*
 * Copyright (c) 2017, GlobalLogic Ukraine LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the GlobalLogic.
 * 4. Neither the name of the GlobalLogic nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY GLOBALLOGIC UKRAINE LLC ``AS IS`` AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GLOBALLOGIC UKRAINE LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Define prior to inclusion of any Linux Kernel header file.
 * This specifies prefix used by pr_*() family functions.
 */
#define pr_fmt(fmt)	KBUILD_MODNAME ": " fmt

#define USE_SPINLOCK

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/atomic.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/random.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/sched.h>
#ifdef USE_SPINLOCK
#include <linux/spinlock.h>
#endif

/* We depend on test3.ko to schedule jobs on specific CPUs for
 * demonstration purposes.
 */
#include <test3.h>

/* This is here synchronization implementations reside.
 *
 *   USE_SPINLOCK   - use spinlocks to synchronize access to shared data
 *                    between tasklet and work.
 *   USE_DISABLE_BH - use local_bh_disable()/local_bh_enable() and
 *                    tasklet_unlock_wait().
 *   none of above  - use tasklet_disable()/tasklet_enable(), optimal.
 */
#include <test4.h>

/* Note that we use pr_debug() excessively to get debugging messages
 * to ensure proper operations. Configuring kernel with dynamic
 * debugging support is recommended (i.e. CONFIG_DYNAMIC_DEBUG=y).
 * To activate ALL pr_debug() on module load use "insmod test4.ko dyndbg==p".
 *
 * Use top(1) on machine where you running this module to see impact
 * on memory/cpu utilization.
 *
 * Use echo {0|1} >/sys/devices/system/cpu/cpu1/online to simulate
 * hotplug events when compiled with CONFIG_HOTPLUG_CPU.
 *
 * It is assumed that this module is running on SMP system with at
 * least 2 CPUs for hotplug demonstration purposes (add -smp cpus=2)
 * and at least 2GB of RAM. Overwise OOM conditions might happen
 * and CONSUMER_DELAY_SECS must be adjusted.
 */

/* How often to run consumer work. Play with this
 * value and you may trigger Out-Of-Memory (OOM)
 * conditions when delayed work running too seldom.
 */
#define CONSUMER_DELAY_SECS	5

/* Keep statistics on number of items handled by
 * producer/consumer tasks.
 *
 * In general using atomic variables for high load
 * access is a poor practice for performance reasons
 * due to high number of memory bus locks. Correct
 * approach is to use per-CPU variables.
 *
 * For our purposes where delayed work run each
 * CONSUMER_DELAY_SECS using atomic is correct:
 * it wont produce high amount of memory bus locking
 * even when running each second.
 *
 * Use ATOMIC_LONG_INIT() here for convenience since
 * .bss section initialize data with zeroes anyway.
 */
static atomic_long_t nr_items_processed = ATOMIC_LONG_INIT(0);

/* Share data accross all possible (supported by the
 * kernel at compile time) CPUs.
 */
static struct my_data_head md_global[NR_CPUS];

static jfd_handle_t jobs;

/* Pure pointer math with BUG_ON() to ease debugging */
static inline int mdh2cpu(const struct my_data_head *mdh)
{
	ptrdiff_t diff = (void *)mdh - (void *)md_global;

	BUG_ON(diff % sizeof(*mdh));
	return diff / sizeof(*mdh);
}

/* This tasklet run on per-CPU basis, possibly in
 * parallel on system with true parellelism.
 *
 * There is no shared data between tasklet instances
 * as well as same tasklet can't run on multiple
 * processors simultaneously.
 */
static void producer_tasklet_func(unsigned long data)
{
	struct my_data_head *mdh = (struct my_data_head *)data;
	struct my_data_list *mdl;
	unsigned int real_cpu, cpu = mdh2cpu(mdh);

	/* Q: does this function thread-safe, reentrant?
	 * Q: should tasklet function be reentrant and/or thread-safe in general?
	 */

	/* Tasklets as well as hard/soft irq isn't preemptible
	 * even if kernel configured with CONFIG_PREEMPT=y.
	 *
	 * But in case of hot-remove scheduled tasklets from
	 * that CPU migrate to one of the available and thus
	 * might run on different CPU.
	 */
	real_cpu = smp_processor_id();
	if (unlikely(real_cpu != cpu)) {
		/* tasklets are running too often: ratelimit message output */
		pr_debug_ratelimited("running producer on cpu%u which differ "
				     "from cpu%u we produce data\n",
				     real_cpu,
				     cpu);
	}

	mdl = kmalloc(sizeof(*mdl), GFP_ATOMIC);
	if (mdl) {
		mdl->val = prandom_u32();
		/* This is critical section in tasklet.
		 *
		 * Delays, sufficient algorithm complexity
		 * would result in increased latencies
		 * and affect overall system responsiveness.
		 *
		 * Keep as fast as possible.
		 */
		exclusive_lock_in_tasklet(mdh);
		list_add_tail(&mdl->list, &mdh->head);
		mdh->nr_items++;
		exclusive_unlock_in_tasklet(mdh);
	}

	/* Rescheduling tasklet too often is worse thing, but we do
	 * this for demonstration purposes here.
	 */
	if (likely(is_job_running(cpu, jobs)))
		tasklet_schedule(&mdh->tasklet);
}

static inline void reschedule_delayed_work(struct delayed_work *dwork)
{
	/* Reschedule itself on the same CPU we scheduled originally */
	schedule_delayed_work_on(dwork->cpu, dwork, CONSUMER_DELAY_SECS * HZ);
}

/* This work run on per-CPU basis, possibly in
 * concurrently with producer tasklet. There is no
 * shared data between works, except statistics
 * of processed items.
 */
static void consumer_dwork_func(struct work_struct *work)
{
	struct delayed_work *dwork = to_delayed_work(work);
	struct my_data_head *mdh = container_of(dwork, struct my_data_head, dwork);
	struct my_data_list *mdl, *tmp;
	unsigned long nr_items;
	unsigned int cpu = mdh2cpu(mdh);
	LIST_HEAD(head);

	/* Q: does this function is thread-safe, reentrant?
	 * Q: should work function be reentrant and/or thread-safe in general?
	 */

	/* Workqueues, as most of user context could be preempted if
	 * kernel is build preemptible (i.e. with CONFIG_PREEMPT=y).
	 * Even using dwork->cpu to determine CPU change is not safe
	 * as we can preempt at any given point (right in if () statement).
	 *
	 * This also why we use "for" instead "on" in debugging messages
	 * as consumer dwork might change CPU it runs on at any time
	 * in the middle of the consuming.
	 */
	if (unlikely(dwork->cpu != cpu)) {
		pr_debug("running consumer on cpu%u which differ"
			 "from cpu%u we consume data for\n",
			 dwork->cpu,
			 cpu);
	}

	/* Q: why not to use "entering consumer dwork for cpu%u\n" and
	 *    similar in rest of the places?
	 */
	pr_debug("%s consumer dwork for cpu%u\n", "entering", cpu);

	/* Take snapshot of the list to work locally.
	 *
	 * Q: why taking snapshot is better here rather than
	 *    consuming in critical section?
	 *
	 * Hint: consider algorithm complexity when
	 *       iternating over list of entries and
	 *       what happening if we do so in critical section.
	 *
	 *       another hint is to turn on pr_debug()
	 *       (e.g. via dynamic debug facility and timing
	 *        in kernel ring buffer messages (e.g. shown
	 *        via dmesg(1)) and compare starting/stopping
	 *       times. You will notice that time spent
	 *       while consuming snapshot of data is very
	 *       significant for kernel times.
	 */
	exclusive_lock_in_dwork(mdh);
	/* Q: what state of the &mdh->head after list_replace_init()? */
	list_replace_init(&mdh->head, &head);
	nr_items = mdh->nr_items;
	mdh->nr_items = 0;
	exclusive_unlock_in_dwork(mdh);

	pr_debug("%s data consumption for cpu%u\n", "starting", cpu);

	/* Update statistics.
	 * Q: why nr_items usage without lock is safe here?
	 * Q: what is the size nr_items_processed on 32/64 bit?
	 * Q: how this affects statistics wraparound and how
	 *    to fix this by using another atomic type variant?
	 */
	atomic_long_add(nr_items, &nr_items_processed);

	list_for_each_entry_safe(mdl, tmp, &head, list) {
		/* Q: why deleting item from the list is
		 *    optional here for this implementation?
		 */
		kfree(mdl);
		/* Avoid other processes starvation due to
		 * excessive number of data processing within
		 * this loop. In general, complexity of this
		 * consumtion is O(n) that may consume lot
		 * of CPU time without givin quantum of time
		 * to other processes leading to increased
		 * latencies and responsiveness.
		 *
		 * By conditionally rescheduling when appropriate
		 * we reduce latencies of other process scheduling
		 * (e.g. other workqueue functions, kthreads,
		 * userspace processes).
		 *
		 * Note that this does not affect responsiveness
		 * of hard/soft irqs as they interrupt user context
		 * here (no local_irq_disable()/local_irq_enable()
		 * and no local_bh_disable()/local_bh_enable() in
		 * this iterator).
		 */
		cond_resched();
	}

	pr_debug("%s data consumption for cpu%u\n", "stopping", cpu);

	/* This is due to timer used with delayed work: we
	 * need to prevent timer from being rescheduled.
	 *
	 * Consider scenario when flush_delayed_work()
	 * calls del_timer_sync() and then queues work
	 * for immediate execution.
	 *
	 * Without is_job_running() check we will rearm
	 * timer and make flush_delayed_work() useless
	 * since it can't catch timer rearms in work
	 * functions (e.g. this one).
	 *
	 * Of course same approach should be taken when
	 * consumer is timer handling function.
	 */
	if (likely(is_job_running(cpu, jobs))) {
		pr_debug("%s consumer dwork for cpu%u\n", "rescheduling", cpu);
		reschedule_delayed_work(dwork);
	}

	pr_debug("%s consumer dwork for cpu%u\n", "leaving", cpu);
}

/* Called with preemption disabled to guarantee we not changing
 * CPU in the middle of scheduling.
 */
static void start_jobs_on_cpu(unsigned int cpu)
{
	struct my_data_head *mdh = &md_global[cpu];

	/* This is however trick, rather than best practice:
	 * kernel API might change to use dwork->cpu in
	 * different way at any time without notice.
	 *
	 * Thus this might introduce problems when porting
	 * to such kernels.
	 *
	 * Better is to pass cpu to reschedule_delayed_work().
	 */
	pr_debug("scheduling %s on cpu%u\n", "dwork", cpu);
	mdh->dwork.cpu = cpu;
	reschedule_delayed_work(&mdh->dwork);

	/* Q: why scheduling tasklet after dwork is preferred
	 *    but not strictly required?
	 */
	pr_debug("scheduling %s on cpu%u\n", "tasklet", cpu);
	tasklet_schedule(&mdh->tasklet);
}

static void stop_jobs_on_cpu(unsigned int cpu)
{
	struct my_data_head *mdh = &md_global[cpu];

	/* Stop producer first to handle remaining
	 * data after it finishes.
	 *
	 * Note that our tasklet reschedules itself,
	 * which is supported for tasklets with one
	 * major restriction:
	 *  tasklet should stop reschedule itself
	 *  before calling tasklet_kill().
	 *
	 * This is done via is_job_running().
	 *
	 * As we already see in delayed work similar
	 * approach applicable not only for tasklets
	 * but also for timers:
	 *  one need to stop rescheduling before
	 *  deleting them.
	 */
	pr_debug("kill tasklet for cpu%u\n", cpu);
	tasklet_kill(&mdh->tasklet);

	/* Q: why do we need to schedule delayed work func again?
	 * Q: how schedule_work() differs from schedule_delayed_work()
	 *    in implementation?
	 *
	 * Schedule using WORK_CPU_UNBOUND in case of cpu isn't
	 * online (called from cpu hotplug notifier).
	 *
	 * Don't want to open code WORK_CPU_UNBOUND to
	 * schedule_delayed_work_on() to keep compatibility
	 * with kernel API that may change WORK_CPU_UNBOUND
	 * meaning or schedule_delayed_work_on() at any time.
	 *
	 * Compiler shall optimize two schedule_delayed_work*()
	 * inlines as they both lead to queue_delayed_work_on()
	 * call.
	 */
	pr_debug("schedule dwork once again for cpu%u\n", cpu);
	if (cpu_online(cpu))
		schedule_delayed_work_on(cpu, &mdh->dwork, 0);
	else
		schedule_delayed_work(&mdh->dwork, 0);

	/* Flush delayed work to ensure we wait for it's completion.
	 * Note that schedule_delayed_work*() with @delay == 0 doesn't
	 * mean job will start immediately: it will be just queued
	 * for run immediately without timer start.
	 */
	pr_debug("flushing delayed work for cpu%u\n", cpu);
	flush_delayed_work(&mdh->dwork);

	/* Ensure consumer (delayed work) not leaving active items.
	 * This is for debugging purposes to prove correctness.
	 */
	BUG_ON(!list_empty(&mdh->head));
	BUG_ON(mdh->nr_items);
}

#ifdef CONFIG_HOTPLUG_CPU
static int cpu_callback(struct notifier_block *nfb, unsigned long action,
			void *hcpu)
{
	unsigned long _cpu = (unsigned long)hcpu;
	/* implicit typecast to make compiler happy with %u in pr_debug() */
	unsigned int cpu = _cpu;

	switch (action) {
	case CPU_ONLINE:
		pr_debug("cpu%u is online: scheduling jobs on it\n", cpu);
		schedule_job_on_cpu(cpu, jobs);
		break;
	case CPU_DEAD:
	case CPU_DEAD_FROZEN:
		pr_debug("cpu%u is offline: finishing jobs on it\n", cpu);
		finish_job_on_cpu(cpu, jobs);
		break;
	}
	return NOTIFY_OK;
}

static struct notifier_block cpu_nb = {
	.notifier_call	= cpu_callback,
};
#endif /* CONFIG_HOTPLUG_CPU */

int __init init_module(void)
{
	struct my_data_head *mdh;
	unsigned int cpu;

	/* Init jobs. Functionality provided by external module (test3.ko) */
	jobs = init_jobs(start_jobs_on_cpu, stop_jobs_on_cpu, GFP_KERNEL);
	if (IS_ERR(jobs)) {
		pr_err("init_jobs() failed\n");
		return PTR_ERR(jobs);
	}

	/* We are going to initialize per-CPU data structures for
	 * all possible (supported by the kernel and specified at
	 * compile time via NR_CPUS define) CPUs.
	 *
	 * Q: why do we need to initialize data for all possible
	 *    CPUs instead of just currently online?
	 */
	for_each_possible_cpu(cpu) {
		mdh = &md_global[cpu];

		tasklet_init(&mdh->tasklet, producer_tasklet_func,
			     (unsigned long)mdh);
		INIT_DELAYED_WORK(&mdh->dwork, consumer_dwork_func);
		INIT_LIST_HEAD(&mdh->head);
		/* No need to initialize nr_items with zero because data
		 * is in .bss section.
		 */
#ifdef USE_SPINLOCK
		spin_lock_init(&mdh->lock);
#endif
	}

#ifdef CONFIG_HOTPLUG_CPU
	/* We want to handle CPU hotplug events to start
	 * tasklets/works on newly coming CPUs or kill them on
	 * ones that go offline.
	 *
	 * Note that it is safe it hotplug event happens
	 * before get_online_cpus() as all required data
	 * for scheduling jobs already prepared and calling
	 * schedule_job_on() in for_each_online_cpu()
	 * is safe as routine is reentrant (just wont
	 * to start job again).
	 */
	register_cpu_notifier(&cpu_nb);
#endif

	/* Make sure no hotplug event happens before
	 * we initially schedule works. This isn't
	 * problem for hot-add CPU, but might be
	 * problematic for hot-remove.
	 *
	 * Due to nature of work scheduling/queuing
	 * we still can start on CPU other than we
	 * initially schedule.
	 */
	get_online_cpus();

	/* But registering only on currently online CPUs */
	for_each_online_cpu(cpu)
		schedule_job_on_cpu(cpu, jobs);

	put_online_cpus();

	return 0;
}

void __exit cleanup_module(void)
{
	unsigned int cpu;

	/* Same consuderations as in module_init() funciton.
	 * Additionally we do not want to CPU go offline before
	 * we cancel job on it: race is possible between
	 * unregister_cpu_notifier() and for_each_online_cpu().
	 *
	 * Alternative approach is to use for_each_possible_cpu()
	 * because finish_job_on_cpu() is safe to use for CPU
	 * where job isn't scheduled (thanks to it's FSM). We
	 * exploit this approach in module_init() function.
	 */
	get_online_cpus();

#ifdef CONFIG_HOTPLUG_CPU
	unregister_cpu_notifier(&cpu_nb);
#endif

	for_each_online_cpu(cpu)
		finish_job_on_cpu(cpu, jobs);

	put_online_cpus();

	/* Release jobs after we finish using them: since
	 * jobs uses global/static shared storage to keeping
	 * it's state we need to release this area prior to
	 * using jobs API.
	 *
	 * This served as an example of not thread-safe code!
	 */
	fini_jobs(jobs);

	/* Finally report statistics about processed items. */
	pr_info("nr_items_processed: %lu\n",
		atomic_long_read(&nr_items_processed));
}
