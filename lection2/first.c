#include <linux/init.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/kernel.h>

#define VERSION "1.2"

MODULE_AUTHOR("Alex S. <aliaksandr.savenok@globallogic.com");
MODULE_DESCRIPTION("[kernel training] lection2 first");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_VERSION(VERSION);

void print_hello(int times) 
{
	printk(KERN_INFO "{first} print_hello:%d!\n", times);
}

EXPORT_SYMBOL(print_hello);

/* Module init/exit is optional for this homework.
 * If present - not a problem.
 */
static int __init first_init(void)
{
	printk(KERN_INFO "{first} init. module version:%s\n", VERSION);
	return 0;
}

static void __exit first_exit(void)
{
	printk(KERN_INFO "{first} exit!\n");
}

module_init(first_init);
module_exit(first_exit);
