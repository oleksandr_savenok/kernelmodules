/* second.c */
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/types.h>

#include <first.h>

#define VERSION "1.2"

MODULE_AUTHOR("Alex S. <aliaksandr.savenok@globallogic.com");
MODULE_DESCRIPTION("[kernel training] lection2 second");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_VERSION(VERSION);

static unsigned int count = 1;

module_param(count, int, S_IRUSR);
MODULE_PARM_DESC(count, "Number of times to print hello message");

static void increase_ref_count(void)
{
	printk(KERN_INFO "{second} __module_get() before:ref:%d\n", module_refcount(THIS_MODULE));
	__module_get(THIS_MODULE);
	printk(KERN_INFO "{second} __module_get() after:ref:%d\n", module_refcount(THIS_MODULE));
}

static int __init second_init(void)
{
	printk(KERN_INFO "{second} init\n");
	printk(KERN_INFO "{second} count %d\n", count);

	WARN_ON(!count);

	BUG_ON(count > 10);

	if (count == 5)
	{
		return -EINVAL;
	}

	if (count == 2)
	{
		increase_ref_count();
	}

	return 0;
}

static void __exit second_exit(void)
{
	int i;

	if (count == 3)
	{
		u8 *kmalloc_pointer = (u8 *)__kmalloc;
		printk(KERN_INFO "{first} _kmalloc value:before: %08x\n", *kmalloc_pointer);
		/* invert 0th bit */
		(* kmalloc_pointer) ^= 1 << 0;
		printk(KERN_INFO "{first} _kmalloc value:after:  %08x\n", *kmalloc_pointer);
	}

	for (i = 0; i < count; ++i)
	{
		print_hello(count);
	}

	printk(KERN_INFO "{second} exit!\n");
}

module_init(second_init);
module_exit(second_exit);
