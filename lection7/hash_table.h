#ifndef _HASH_TABLE_H
#define _HASH_TABLE_H

#include <linux/types.h>
#include <linux/list.h>

struct htable_config {
	u32	initval;

	u32	(*hkey)(const struct hlist_node *n, u32 initval, u8 order);
	int	(*hcmp)(const struct hlist_node *n1,
			const struct hlist_node *n2);
	void	(*hfree)(const struct hlist_node *n);
};

struct htable {
	/* better to align member using tabls: code style
	 * struct hlist_node	buckets;
	 * struct htable_config	cfg;
	 * u8					order;
	 *
	 * alex : looks like a bug of code editor of bitbucket,
	 * when I press "edit" tabs are shifted, in view mode
	 * or in vim editor it looks ok.
	 */
	struct hlist_head	*buckets;
	struct htable_config	cfg;
	u8			order;
};

struct htable *alloc_htable(u8 order, const struct htable_config *cfg);
void free_htable(struct htable *t);
struct htable *resize_htable(struct htable *t);
struct hlist_node *find_htable_node(const struct htable *t,
				    const struct hlist_node *n);
void insert_htable_node(struct htable *t, struct hlist_node *n);
void flush_htable(struct htable *t);
void destroy_htable(struct htable *t);

static inline u32 hash_key(const struct htable *t,
			   const struct hlist_node *n)
{
	return t->cfg.hkey(n, t->cfg.initval, t->order);
}

static inline void hash_free(const struct htable *t,
			     const struct hlist_node *n)
{
	t->cfg.hfree(n);
}

static inline int hash_compare(const struct htable *t,
			       const struct hlist_node *n1,
			       const struct hlist_node *n2)
{
	return t->cfg.hcmp(n1, n2);
}

static inline struct hlist_head *get_bucket(const struct htable *t,
					    const struct hlist_node *n)
{
	u32 hash = hash_key(t, n);

	return &t->buckets[hash];
}

#endif /* _HASH_TABLE_H */

