#include <linux/module.h>
#include <linux/list.h>
#include <linux/jhash.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/printk.h>

#include <hash_table.h>

#define MAX_ORDER_VALUE 31

static bool is_table_valid(const struct htable *table)
{
	return table && table->buckets;
}

struct htable *alloc_htable(u8 order, const struct htable_config *cfg)
{
	struct htable *table;
	u32 i, buckets_count = jhash_size(order);

	if (order > MAX_ORDER_VALUE)
		return NULL;

	if (!cfg || !cfg->hcmp || !cfg->hkey || !cfg->hfree)
		return NULL;

	table = kmalloc(sizeof(*table), GFP_KERNEL);
    /* do not insert spaces between kmalloc() and if (try ... check): code style */
	if (!table)
		return NULL;

	pr_debug("buckets_count:%u\n", buckets_count);

	table->cfg = *cfg;
	table->order = order;
	table->buckets = kmalloc_array(buckets_count,
				sizeof(*table->buckets), GFP_KERNEL);
	if (!table->buckets) {
		kfree(table);
		return NULL;
	}

	for (i = 0; i < buckets_count; i++)
		INIT_HLIST_HEAD(&table->buckets[i]);

	return table;
}

void free_htable(struct htable *table)
{
	if (!table)
		return;

	kfree(table->buckets);
	kfree(table);
}

struct htable *resize_htable(struct htable *old_table)
{
	struct htable *new_table;
	u32 i, buckets_count;

	if (!is_table_valid(old_table))
		return NULL;

	new_table = alloc_htable(old_table->order + 1, &old_table->cfg);
	if (!new_table)
		return NULL;

	buckets_count = jhash_size(old_table->order);
	for (i = 0; i < buckets_count; i++) {
		struct hlist_head *head, *bucket;
		struct hlist_node *tmp, *pos;

		head = &old_table->buckets[i];
		hlist_for_each_safe(pos, tmp, head) {
			hlist_del(pos);
			bucket = get_bucket(new_table, pos);
			hlist_add_head(pos, bucket);
		}
	}

	free_htable(old_table);

	return new_table;
}

struct hlist_node *find_htable_node(const struct htable *table,
				const struct hlist_node *searched_node)
{
	struct hlist_head *head;
	struct hlist_node *current_node;

	if (!is_table_valid(table))
		return NULL;

	head = get_bucket(table, searched_node);
	hlist_for_each(current_node, head) {
		/* use inline helper here */
		if (!hash_compare(table, current_node, searched_node))
			return current_node;
	}

	return NULL;
}

void insert_htable_node(struct htable *table, struct hlist_node *new_node)
{
	struct hlist_head *bucket;

	if (!is_table_valid(table))
		return;

	/* correct */
	if (find_htable_node(table, new_node))
		return;

	bucket = get_bucket(table, new_node);
	hlist_add_head(new_node, bucket);
}

void flush_htable(struct htable *table)
{
	u32 i, buckets_count;

	if (!is_table_valid(table))
		return;

	buckets_count = jhash_size(table->order);
	for (i = 0; i < buckets_count; i++) {
		struct hlist_head *head;
		struct hlist_node *n, *pos;

		head = &table->buckets[i];
		hlist_for_each_safe(pos, n, head) {
			hlist_del(pos);
			hash_free(table, pos);
		}
	}
}

void destroy_htable(struct htable *table)
{
	flush_htable(table);
	free_htable(table);
}
