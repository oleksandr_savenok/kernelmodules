#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/random.h>
#include <linux/slab.h>
#include <linux/jhash.h>

#include <hash_table.h>

#define VERSION "1.4"

#define ORDER 4
#define ITEMS_COUNT 10000

MODULE_AUTHOR("Alex S. <aliaksandr.savenok@globallogic.com>");
MODULE_DESCRIPTION("[kernel training] lection7 hash_table_test.");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_VERSION(VERSION);

struct my_data {
	u32 val;
	struct hlist_node hlist;
};

static u32 my_data_jhash(const struct hlist_node *n, u32 initval, u8 order)
{
	struct my_data *data = hlist_entry(n, struct my_data, hlist);

	/* thats right to use for performance reasons */
	return jhash_1word(data->val, initval) & jhash_mask(order);
}

static int my_data_hcmp(const struct hlist_node *n1,
			const struct hlist_node *n2)
{
	struct my_data *data1 = hlist_entry(n1, struct my_data, hlist);
	struct my_data *data2 = hlist_entry(n2, struct my_data, hlist);

	return (int)(data1->val > data2->val) - (int)(data1->val < data2->val);
}

static void my_data_hfree(const struct hlist_node *n)
{
	struct my_data *data = hlist_entry(n, struct my_data, hlist);

	kfree(data);
}

static unsigned int max_chain_htable(struct htable *table)
{
	unsigned int i;
	unsigned int index = 0;
	unsigned int max_len = 0;
    /* drop new lines in local variable declarations: code style */
    /* place structures first, then unsigned integers: code style */
	struct hlist_head *head;
	struct hlist_node *n;
	struct my_data *data;
	u32 i, buckets_count;
	u32 index = 0;
	u32 max_len = 0;

	if (!table)
		return 0;

	buckets_count = jhash_size(table->order);
	for (i = 0; i < buckets_count; i++) {
		unsigned int count = 0;

		head = &table->buckets[i];
		hlist_for_each(n, head) {
			count++;
		}

		if (count > max_len)
			index = i;

		/* right use of the preprocessor macro */
		max_len = max(max_len, count);
	}

	/* good point for debugging */
	pr_debug("max chain. index:%u\n", index);

	head = &table->buckets[index];
	hlist_for_each(n, head) {
		data = hlist_entry(n, struct my_data, hlist);
		if (data)
			pr_debug("%u\n", data->val);
	}

	return max_len;
}

struct htable_config config __initdata = {
	/* adjust initialziers with tabs: same as when declaring members
	 * of data structure: code style, make code readable.
	 *
	 * alex: didn't understand where exactly tab is missing?
	 * between = and [value], ?
	 */
	.hkey	= my_data_jhash,
	.hcmp	= my_data_hcmp,
	.hfree	= my_data_hfree,
};

static int __init hash_table_test_init(void)
{
	unsigned int i, max_len;

	struct htable *table;

	get_random_bytes(&config.initval, sizeof(config.initval));
	table = alloc_htable(ORDER, &config);
	if (!table)
		return -ENOMEM;

	for (i = 0; i < ITEMS_COUNT; ++i) {
		struct my_data *data;

		data = kmalloc(sizeof(*data), GFP_KERNEL);

		if (!data) {
			/* correct! */
			flush_htable(table);
			return -ENOMEM;
		}

		data->val = prandom_u32();
		/* insert_htable_node() should be aware for
		 * case where data->val =~ htable (i.e. prandom_u32()
		 * returns same data two or more times.
		 * see corresponding comments in hash_table.c
		 */
		insert_htable_node(table, &data->hlist);
	}

	max_len = max_chain_htable(table);
	pr_info("Max hash table chain length is %u\n", max_len);

	table = resize_htable(table);

	max_len = max_chain_htable(table);
	pr_info("Max hash table chain length is %u\n", max_len);

	flush_htable(table);

	return 0;
}

static void __exit hash_table_test_exit(void)
{
	pr_info("exit.\n");
}

module_init(hash_table_test_init);
module_exit(hash_table_test_exit);
