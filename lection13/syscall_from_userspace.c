#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <my_syscall.h>

#define __NR_my_syscall 332

#define DEFAULT_NAME "This is a name!"

int main(void)
{
	unsigned long count;

	printf("number: %d\n", __NR_my_syscall);

	long res = syscall(__NR_my_syscall, 10, 0, 0);
	printf("system call returned %s.\n", strerror(res));

	struct my_syscall_in_data in_data;

	int name_len = strlen(DEFAULT_NAME) + 1;

	in_data.name = (my_string_t *)malloc(sizeof(size_t) + name_len * sizeof(char));
	strncpy(in_data.name->str, DEFAULT_NAME, name_len);
	in_data.name->str[name_len] = '\0';
	in_data.name->len = name_len;
	in_data.val = 128;

	printf("in_data->name->str: %s\n", in_data.name->str);
	printf("in_data->name->len: %lu\n", in_data.name->len);
	printf("in_data->val: %u\n", in_data.val);

	int out_name_len = OUT_BUFFER_MAX_SIZE;
	printf("out_name_len: %u\n", out_name_len);

	struct my_syscall_out_data out_data;
	out_data.name = (my_string_t *)malloc(sizeof(size_t) + out_name_len * sizeof(char)); 

	res = syscall(__NR_my_syscall, 1, &in_data, &out_data);

	printf("system call returned %s.\n", strerror(res));

	printf("sysname: %s.\n", out_data.uts_name.sysname);
	printf("length: %lu.\n", out_data.name->len);
	printf("name: %s.\n", out_data.name->str);

	for (int i = 0; i < RAND_SIZE; i++) {
		printf("rand %d: %u\n", i, out_data.rand_data[i]);
	}

	/******** API 2 ************************/

	struct my_syscall_in_data2 in_data2;

	in_data2.name = (my_string_t *)malloc(sizeof(size_t) + name_len * sizeof(char));
	strncpy(in_data2.name->str, DEFAULT_NAME, name_len);
	in_data2.name->str[name_len] = '\0';
	in_data2.name->len = name_len;
	in_data2.val = 128;
	in_data2.val2 = 256;

	printf("in_data2->name->str: %s\n", in_data2.name->str);
	printf("in_data2->name->len: %lu\n", in_data2.name->len);
	printf("in_data2->val: %u\n", in_data2.val);
	printf("in_data2->val: %u\n", in_data2.val2);

	struct my_syscall_out_data2 out_data2;
	out_data2.name = (my_string_t *)malloc(sizeof(size_t) + out_name_len * sizeof(char)); 

	res = syscall(__NR_my_syscall, 2, &in_data2, &out_data2);

	printf("system call returned %s.\n", strerror(res));

	printf("sysname: %s.\n", out_data2.uts_name.sysname);
	printf("length: %lu.\n", out_data2.name->len);
	printf("name: %s.\n", out_data2.name->str);

	for (int i = 0; i < RAND_SIZE; i++) {
		printf("rand %d: %u\n", i, out_data2.rand_data[i]);
	}

	for (int i = 0; i < RAND_SIZE; i++) {
		printf("rand %d: %lu\n", i, out_data2.rand_data2[i]);
	}

	free(in_data.name);
	free(out_data.name);
	free(in_data2.name);
	free(out_data2.name);

	return res;
}
