#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/printk.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/err.h>

#define VERSION "1.5"

#define NAME_LENGTH 64
#define ITEMS_COUNT 3

/* Note that macro function (e.g. with ({) can be used as
 * following:
 *
 * #ifdef SOME_CHECK
 * #define FUNC_MACRO(condition) ({ \
 *   int __ret = 0;                 \
 *   if (condition)                 \
 *     __ret = 1;                   \
 *   __ret;                         \
 * })
 * #else
 * #define FUNC_MACRO(condition) (0)
 * #endif
 *
 * static void func(int condition)
 * {
 *     int i = FUNC_MACRO(condition);
 *
 *     ...
 */

#define PANIC_ON(condition)					\
	do {							\
		if (condition) {				\
			pr_emerg("List data doesn't released.");\
			dump_stack();				\
			panic("Call panic from macros.");	\
		}						\
	} while (0)						\

MODULE_AUTHOR("Alex S. <aliaksandr.savenok@globallogic.com>");
MODULE_DESCRIPTION("[kernel training] lection4 list_example.");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_VERSION(VERSION);

struct item {
	unsigned int id;
	char name[NAME_LENGTH];
	struct list_head entry;
};

struct list_head *head;

/* Well done! */
static void remove_items(struct list_head *head)
{
	struct item *current_item, *tmp;

	if (!head)
		return;

	list_for_each_entry_safe(current_item, tmp, head, entry) {
		pr_debug("removing item id:%u name:%s\n",
			current_item->id, current_item->name);
		list_del(&current_item->entry);
		kfree(current_item);
	}
}

static struct list_head *init_items(unsigned int items_count)
{
	unsigned int i;


	struct list_head *head_local = kmalloc(sizeof(*head_local), GFP_KERNEL);

	if (!head_local)
		goto errout;

	INIT_LIST_HEAD(head_local);

	for (i = 0; i < items_count; ++i) {

		int count;

		struct item *new_item = kzalloc(sizeof(*new_item), GFP_KERNEL);

		if (!new_item)
			goto errout;

		new_item->id = i + 1;

		/* Good catch of misconfigured ->name[] length */
		BUILD_BUG_ON(sizeof(new_item->name) <
					sizeof("name: 424967295"));

		count = snprintf(new_item->name, sizeof(new_item->name),
					"name:%u", new_item->id);

		if (count < 0)
			goto errout;

		list_add_tail(&new_item->entry, head_local);

	}

	return head_local;
errout:
	remove_items(head_local);
	kfree(head_local);
	return ERR_PTR(-ENOMEM);
}

static int __init list_example_init(void)
{
	pr_info("init. module version:%s\n", VERSION);

	head = init_items(ITEMS_COUNT);

	return PTR_ERR_OR_ZERO(head);
}

static void __exit list_example_exit(void)
{
	remove_items(head);

	PANIC_ON(!list_empty(head));

	kfree(head);

	pr_info("exit!\n");
}

module_init(list_example_init);
module_exit(list_example_exit);