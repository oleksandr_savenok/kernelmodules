qemu-system-x86_64 \
	-m 1024M \
	-kernel ~/kernel/build4.9/arch/x86/boot/bzImage \
	-hda ~/kernel/hdd.img \
	-append "root=/dev/sda console=ttyS0 mem=512M memmap cma=128M bootmem_debug" --enable-kvm --nographic

